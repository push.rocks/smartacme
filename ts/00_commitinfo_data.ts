/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartacme',
  version: '4.0.3',
  description: 'acme with an easy yet powerful interface in TypeScript'
}
